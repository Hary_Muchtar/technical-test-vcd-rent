﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using WebMVC.Data.Base;
using WebMVC.Data.Model;

namespace WebMVC.DataRepository.Repo
{
    public interface IPackageRepository : IRepository<tblPackage>
    {
        IEnumerable<tblPackage> Read();
        tblPackage getById(int Id);
    }
    public class PackageRepository : IPackageRepository
    {
        private readonly IDataContext context;
        public PackageRepository(IDataContext context)
        {
            this.context = context;
        }

        public IEnumerable<tblPackage> Read()
        {
            return context.tblPackageS.ToList();
        }
        public tblPackage getById(int Id)
        {
            return context.tblPackageS.FirstOrDefault(x => x.PackageId == Id);
        }
        public void Add(tblPackage entity)
        {
            context.tblPackageS.Add(entity);
        }

        public void Attach(tblPackage entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(tblPackage entity)
        {
            context.tblPackageS.Remove(entity);
        }

        public void Detach(tblPackage entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(tblPackage entity)
        {
            string query = ("UPDATE tblPackage SET PackageName = @PackageName, MaxRent = @MaxRent, CostPerMonth = @CostPerMonth WHERE PackageId = @PackageId");
            context.Context.Database.ExecuteSqlCommand(query, new SqlParameter("@PackageId", entity.PackageId), new SqlParameter("@PackageName", entity.PackageName), new SqlParameter("@MaxRent", entity.MaxRent), new SqlParameter("@CostPerMonth", entity.CostPerMonth));
        }

        public int SaveChanges()
        {
            return context.Context.SaveChanges();
        }
    }
}
