﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using WebMVC.Data.Base;
using WebMVC.Data.Model;
using WebMVC.Data.Model.ViewModel;

namespace WebMVC.DataRepository.Repo
{
    public interface IRentRepository : IRepository<tblRent>
    {
        string cekActiveUser(int id);
        IEnumerable<viewRent> Read();
        SelectList getMember();
    }
    public class RentRepository : IRentRepository
    {
        private readonly IDataContext context;

        public RentRepository(IDataContext context)
        {
            this.context = context;
        }
        public SelectList getMember()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var query = context.tblMemberS.AsEnumerable().ToList();

            list = (from item in query
                    select new SelectListItem
                    {
                        Text = item.FullNameMember.ToString(),
                        Value = item.IdMember.ToString()
                    }).ToList();
            return new SelectList(list, "Value", "Text");
        }
        public string cekActiveUser(int id)
        {
            return context.tblMemberS.FirstOrDefault(x => x.IdMember == id).IsActive;
        }
        public IEnumerable<viewRent>Read()
        {
            var data = (from a in context.tblRentS
                        join b in context.tblMemberS on a.IdMember equals b.IdMember
                        select new viewRent
                        {
                            IdRent = a.IdRent,
                            IdMember = a.IdMember,
                            FullNameMember = b.FullNameMember,
                            Qty = a.Qty
                        });
            return data.ToList();
        }
        public void Add(tblRent entity)
        {
            context.tblRentS.Add(entity);
        }

        public void Attach(tblRent entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(tblRent entity)
        {
            context.tblRentS.Remove(entity);
        }

        public void Detach(tblRent entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(tblRent entity)
        {
            throw new NotImplementedException();
        }

        public int SaveChanges()
        {
            return context.Context.SaveChanges();
        }
    }
}
