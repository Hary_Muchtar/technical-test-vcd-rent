﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.DataRepository.Repo
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);

        void Edit(T entity);

        void Delete(T entity);

        void Attach(T entity);

        void Detach(T entity);

        int SaveChanges();
    }
}
