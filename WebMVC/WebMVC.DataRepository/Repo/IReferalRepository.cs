﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using WebMVC.Data.Base;
using WebMVC.Data.Model;
using WebMVC.Data.Model.ViewModel;

namespace WebMVC.DataRepository.Repo
{
    public interface IReferalRepository : IRepository<tblReferal>
    {
        IEnumerable<tblReferal> Read();
        tblReferal getById(int Id);
        IEnumerable<viewMemberReferal> ReadReport();
        SelectList getMember();
        int cekDataReady(string name, int phoneNo);
    }
    public class ReferalRepository : IReferalRepository 
    {
        private readonly IDataContext context;
        public ReferalRepository(IDataContext context)
        {
            this.context = context;
        }

        public SelectList getMember()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var query = context.tblMemberS.AsEnumerable().ToList();

            list = (from item in query
                    select new SelectListItem
                    {
                        Text = item.FullNameMember.ToString(),
                        Value = item.IdMember.ToString()
                    }).ToList();
            return new SelectList(list, "Value", "Text");
        }

        public int cekDataReady(string name, int phoneNo)
        {
            return context.tblReferalS.Count(x => x.FullNameFriend == name && x.PhoneFriend == phoneNo);
        }
        public IEnumerable<tblReferal> Read()
        {
            return context.tblReferalS.ToList();
        }
        public IEnumerable<viewMemberReferal> ReadReport()
        {
            var data = (from a in context.tblMemberS
                        join b in context.tblReferalS on a.IdMember equals b.IdMember
                        select new viewMemberReferal
                        {
                            IdMember = a.IdMember,
                            FullNameMember = a.FullNameMember,
                            FullNameFriend = b.FullNameFriend,
                            PhoneFriend = b.PhoneFriend,
                            RegistrationDate = a.RegistrationDate,
                            IsActive = a.IsActive
                        });
            data.ToList();//.Where(x=> x.IsActive == "No").OrderByDescending(x=> x.RegistrationDate);
            return data.Where(x => x.IsActive == "No");

        }
        public tblReferal getById(int Id)
        {
            return context.tblReferalS.FirstOrDefault(x => x.IdRefereal == Id);
        }
        public void Add(tblReferal entity)
        {
            context.tblReferalS.Add(entity);
        }

        public void Attach(tblReferal entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(tblReferal entity)
        {
            context.tblReferalS.Remove(entity);
        }

        public void Detach(tblReferal entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(tblReferal entity)
        {
            string query = ("UPDATE TblReferal SET FullNameFriend = @FullNameFriend, PhoneFriend = @PhoneFriend WHERE IdRefereal = @IdRefereal");
            context.Context.Database.ExecuteSqlCommand(query, new SqlParameter("@IdRefereal", entity.IdRefereal), new SqlParameter("@FullNameFriend", entity.FullNameFriend), new SqlParameter("@PhoneFriend", entity.PhoneFriend));
        }

        public int SaveChanges()
        {
            return context.Context.SaveChanges();
        }
    }
}
