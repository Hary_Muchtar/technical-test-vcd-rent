﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using WebMVC.Data.Base;
using WebMVC.Data.Model;

namespace WebMVC.DataRepository.Repo
{
    public interface IKnowRepository:IRepository<tblKnow>
    {
        IEnumerable<tblKnow> Read();
        tblKnow getById(int Id);
    }
    public class KnowRepository : IKnowRepository
    {
        private readonly IDataContext context;
        public KnowRepository(IDataContext context)
        {
            this.context = context;
        }

        public IEnumerable<tblKnow> Read()
        {
            return context.tblKnowS.ToList();
        }
        public tblKnow getById(int Id)
        {
            return context.tblKnowS.FirstOrDefault(x => x.KnowId == Id);
        }
        public void Add(tblKnow entity)
        {
            context.tblKnowS.Add(entity);
        }

        public void Attach(tblKnow entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(tblKnow entity)
        {
            context.tblKnowS.Remove(entity);
        }

        public void Detach(tblKnow entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(tblKnow entity)
        {
            string query = ("UPDATE TblKnow SET KnowFrom = @KnowFrom WHERE KnowId = @KnowId");
            context.Context.Database.ExecuteSqlCommand(query, new SqlParameter("@KnowId", entity.KnowId), new SqlParameter("@KnowFrom", entity.KnowFrom));
        }

        public int SaveChanges()
        {
            return context.Context.SaveChanges();
        }
        
    }
}
