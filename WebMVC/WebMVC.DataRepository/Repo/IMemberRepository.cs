﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using WebMVC.Data.Base;
using WebMVC.Data.Model;
using WebMVC.Data.Model.ViewModel;
using System.Threading;
using System.Threading.Tasks;

namespace WebMVC.DataRepository.Repo
{
    public interface IMemberRepository : IRepository<viewMember>
    {
        SelectList getSourceName();
        SelectList getPackageName();
        IEnumerable<viewMember> Read();
        void AddMember(tblMember entity);
        void AddReferal(tblReferal entity);
        int getMaxIdMember();
        Task<int> SaveChangesAsync();
        tblMember getById(int Id);
        void EditMember(tblMember entity);
    }
    public class MemberRepository : IMemberRepository
    {
        private readonly IDataContext context;
        public MemberRepository(IDataContext context)
        {
            this.context = context;
        }
        public tblMember getById(int Id)
        {
            return context.tblMemberS.FirstOrDefault(x => x.IdMember == Id);
        }
        public SelectList getSourceName()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var query = context.tblKnowS.AsEnumerable().ToList();

            list = (from item in query
                    select new SelectListItem
                    {
                        Text = item.KnowFrom.ToString(),
                        Value = item.KnowId.ToString()
                    }).ToList();
            return new SelectList(list, "Value", "Text");
        }

        public SelectList getPackageName()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var query = context.tblPackageS.AsEnumerable().ToList();

            list = (from item in query
                    select new SelectListItem
                    {
                        Text = item.PackageName.ToString(),
                        Value = item.PackageId.ToString()
                    }).ToList();
            return new SelectList(list, "Value", "Text");
        }

        public IEnumerable<viewMember>Read()
        {
            var data = (from a in context.tblMemberS
                        join b in context.tblKnowS on a.KnowId equals b.KnowId
                        join c in context.tblPackageS on a.PackageId equals c.PackageId
                        select new viewMember
                        {
                            IdMember = a.IdMember,
                            IdentityNumber = a.IdentityNumber,
                            FullNameMember = a.FullNameMember,
                            PhoneMember = a.PhoneMember,
                            Age = a.Age,
                            KnowFrom = b.KnowFrom,
                            PackageName = c.PackageName,
                            RegistrationDate = a.RegistrationDate,
                            IsActive = a.IsActive
                        });
            return data.ToList();
        }

        public void Add(viewMember entity)
        {
            throw new NotImplementedException();
        }
        public void AddMember(tblMember entity)
        {
            context.tblMemberS.Add(entity);
        }
        public void AddReferal(tblReferal entity)
        {
            string query = ("INSERT INTO TblReferal (IdMember, FullNameFriend, PhoneFriend) values (@IdMember, @FullNameFriend, @PhoneFriend)");
            context.Context.Database.ExecuteSqlCommand(
                query, new SqlParameter("@IdMember", entity.IdMember),
                new SqlParameter("@FullNameFriend", entity.FullNameFriend),
                new SqlParameter("@PhoneFriend", entity.PhoneFriend)
                );
        }
        public int getMaxIdMember()
        {
            return context.tblMemberS.Max(x => x.IdMember);
        }
        public void Attach(viewMember entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(viewMember entity)
        {
            throw new NotImplementedException();
        }

        public void Detach(viewMember entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(viewMember entity)
        {
            //string query = ("UPDATE TblMember SET IsActive = @IsActive WHERE IdMember = @IdMember");
            //context.Context.Database.ExecuteSqlCommand(query, new SqlParameter("@IdMember", entity.IdMember), new SqlParameter("@IsActive", entity.IsActive));
        }
        public void EditMember(tblMember entity)
        {
            string query = ("UPDATE TblMember SET IsActive = @IsActive WHERE IdMember = @IdMember");
            context.Context.Database.ExecuteSqlCommand(query, new SqlParameter("@IdMember", entity.IdMember), new SqlParameter("@IsActive", entity.IsActive));
        }
        public int SaveChanges()
        {
            return context.Context.SaveChanges();
        }
        public virtual Task<int> SaveChangesAsync()
        {
            return context.Context.SaveChangesAsync();
        }
    }
}
