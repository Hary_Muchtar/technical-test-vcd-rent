﻿using WebMVC.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMVC.Data.Model.ViewModel;

namespace WebMVC.Data.Base
{
    public interface IDataContext
    {
        DbContext Context { get; }
        DbSet<tblPackage> tblPackageS { get; set; }
        DbSet<tblKnow> tblKnowS { get; set; }
        DbSet<tblMember> tblMemberS { get; set; }
        DbSet<tblReferal> tblReferalS { get; set; }
        DbSet<tblRent> tblRentS { get; set; }
    }
}
