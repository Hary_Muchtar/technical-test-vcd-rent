﻿using WebMVC.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMVC.Data.Base;
using System.Data.Entity.ModelConfiguration.Conventions;
using WebMVC.Data.Model.ViewModel;

namespace WebMVC.Data.Base
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext()
            : base(Common.WebConfig.ConnectionString)
        {

        }
        public DbContext Context => this;
        public DbSet<tblPackage> tblPackageS { get; set; }
        public DbSet<tblKnow> tblKnowS { get; set; }
        public DbSet<tblMember> tblMemberS { get; set; }
        public DbSet<tblReferal> tblReferalS { get; set; }
        public DbSet<tblRent> tblRentS { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        
    }
}
