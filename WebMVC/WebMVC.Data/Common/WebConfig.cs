﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;

namespace WebMVC.Data.Common
{
    public sealed class WebConfig
    {
        public static string ConnectionString
        {
            get
            {
                string server = WebConfigurationManager.AppSettings["DbServer"];
                string userid = WebConfigurationManager.AppSettings["DbUserId"];
                string password = WebConfigurationManager.AppSettings["DbPassword"];
                string database = WebConfigurationManager.AppSettings["DbName"];

                return $"Server={server};Database={database};User Id={userid};Password={password};";
            }
        }
    }
    public class AppSettings
    {
        [Required]
        [AllowHtml]
        [DisplayName("Database Server")]
        public string DbServer { get; set; }

        [Required]
        [DisplayName("Database Password")]
        public string DbPassword { get; set; }

        [Required]
        [DisplayName("Database User ID")]
        public string DbUserId { get; set; }

        [Required]
        [AllowHtml]
        [DisplayName("Database Name")]
        public string DbName { get; set; }


    }
}
