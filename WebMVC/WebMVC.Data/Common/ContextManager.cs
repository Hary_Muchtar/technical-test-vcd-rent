﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Data.Common
{
    public class ContextManager
    {
        private DbContext context;

        public ContextManager(DbContext context)
        {
            this.context = context;
        }

        public void Detach<T>(T entity) where T : class
        {
            if (context.Entry(entity).State != EntityState.Detached)
                (((IObjectContextAdapter)context).ObjectContext).Detach(entity);
        }

        public void Attach<T>(T entity) where T : class
        {
            context.Set<T>().Attach(entity);
        }
    }
}
