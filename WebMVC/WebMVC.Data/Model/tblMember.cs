﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMVC.Data.Model
{
    public class tblMember
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdMember", Order = 0)]
        [DisplayName("ID Member")]
        public int IdMember { get; set; }

        [Column("IdentityNumber", Order = 1)]
        [DisplayName("Identity Number")]
        public int IdentityNumber { get; set; }

        [MaxLength(50)]
        [Column("FullNameMember", Order = 2, TypeName = "varchar"), StringLength(50)]
        [DisplayName("Member Full Name")]
        public string FullNameMember { get; set; }

        [MaxLength(100)]
        [Column("Address", Order = 3, TypeName = "varchar"), StringLength(100)]
        [DisplayName("Address")]
        public string Address { get; set; }

        [Column("PhoneMember", Order = 4)]
        [DisplayName("Member Phone")]
        public int PhoneMember { get; set; }

        [Column("Age", Order = 5)]
        [DisplayName("Age")]
        public int Age { get; set; }

        [Column("AnotherMembership", Order = 6)]
        [DisplayName("Another Membership")]
        public string AnotherMembership { get; set; }

        [Column("KnowId", Order = 7)]
        [DisplayName("Source")]
        public int KnowId { get; set; }

        [Column("PackageId", Order = 8)]
        [DisplayName("Package")]
        public int PackageId { get; set; }

        [Column("RegistrationDate", Order = 9)]
        [DisplayName("Registration Date")]
        public DateTime RegistrationDate { get; set; }

        [Column("IsActive", Order = 10)]
        [DisplayName("Active")]
        public string IsActive { get; set; }

    }
}
