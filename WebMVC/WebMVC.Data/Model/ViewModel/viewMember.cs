﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMVC.Data.Model.ViewModel
{
    public class viewMember
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdMember")]
        [DisplayName("ID Member")]
        public int IdMember { get; set; }

        [Column("IdentityNumber")]
        [DisplayName("Identity Number")]
        public int IdentityNumber { get; set; }

        [MaxLength(50)]
        [Column("FullNameMember", TypeName = "varchar"), StringLength(50)]
        [DisplayName("Member Full Name")]
        public string FullNameMember { get; set; }

        [MaxLength(100)]
        [Column("Address", TypeName = "varchar"), StringLength(100)]
        [DisplayName("Address")]
        public string Address { get; set; }

        [Column("PhoneMember")]
        [DisplayName("Member Phone")]
        public int PhoneMember { get; set; }

        [MaxLength(50)]
        [Column("FullNameFriend1", TypeName = "varchar"), StringLength(50)]
        [DisplayName("Friend Full Name 1")]
        public string FullNameFriend1 { get; set; }

        [Column("PhoneFriend1", Order = 7)]
        [DisplayName("Friend Phone 1")]
        public int PhoneFriend1 { get; set; }

        [MaxLength(50)]
        [Column("FullNameFriend2", TypeName = "varchar"), StringLength(50)]
        [DisplayName("Friend Full Name 2")]
        public string FullNameFriend2 { get; set; }

        [Column("PhoneFriend2")]
        [DisplayName("Friend Phone 2")]
        public int PhoneFriend2 { get; set; }

        [Column("Age")]
        [DisplayName("Age")]
        public int Age { get; set; }
        
        [Column("AnotherMembership"), StringLength(1)]
        [DisplayName("Another Membership")]
        public string AnotherMembership { get; set; }

        [Column("KnowId")]
        [DisplayName("Source")]
        public int KnowId { get; set; }

        [MaxLength(100)]
        [Column("KnowFrom", TypeName = "varchar"), StringLength(100)]
        [DisplayName("Source")]
        public string KnowFrom { get; set; }

        [Column("PackageId")]
        [DisplayName("Package")]
        public int PackageId { get; set; }

        [MaxLength(20)]
        [Column("PackageName", TypeName = "varchar"), StringLength(20)]
        [DisplayName("Package Name")]
        public string PackageName { get; set; }

        [Column("RegistrationDate")]
        [DisplayName("Registration Date")]
        public DateTime RegistrationDate { get; set; }


        [Column("IsActive"), StringLength(1)]
        [DisplayName("Active")]
        public string IsActive { get; set; }
    }
}
