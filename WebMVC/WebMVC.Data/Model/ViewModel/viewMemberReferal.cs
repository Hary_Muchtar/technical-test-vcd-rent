﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Data.Model.ViewModel
{
    public class viewMemberReferal
    {
        [Key]
        [Column("IdMember", Order = 0)]
        [DisplayName("ID Member")]
        public int IdMember { get; set; }

        [MaxLength(50)]
        [Column("FullNameMember", Order = 1, TypeName = "varchar"), StringLength(50)]
        [DisplayName("Member Full Name")]
        public string FullNameMember { get; set; }

        [Column("IdRefereal", Order = 2)]
        [DisplayName("Referal ID")]
        public int IdRefereal { get; set; }

        [Column("FullNameFriend", Order = 3, TypeName = "varchar"), StringLength(50)]
        [DisplayName("Referal Name")]
        public string FullNameFriend { get; set; }

        [Column("PhoneFriend", Order = 4)]
        [DisplayName("Referal Phone")]
        public int PhoneFriend { get; set; }

        [Column("RegistrationDate", Order = 5)]
        [DisplayName("Registration Date")]
        public DateTime RegistrationDate { get; set; }

        [Column("IsActive", Order = 6)]
        [DisplayName("Active")]
        public string IsActive { get; set; }
    }
}
