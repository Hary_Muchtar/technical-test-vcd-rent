﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Data.Model.ViewModel
{
    public class viewRent
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdRent")]
        [DisplayName("ID Rent")]
        public int IdRent { get; set; }

        [Column("IdMember")]
        [DisplayName("Member")]
        public int IdMember { get; set; }

        [MaxLength(50)]
        [Column("FullNameMember", TypeName = "varchar"), StringLength(50)]
        [DisplayName("Member Full Name")]
        public string FullNameMember { get; set; }

        [Column("Qty")]
        [DisplayName("Quantity")]
        public int Qty { get; set; }
    }
}
