﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Data.Model
{
    public class tblReferal
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdRefereal", Order = 0)]
        [DisplayName("Referal ID")]
        public int IdRefereal { get; set; }

        [DisplayName("Member ID")]
        public int IdMember { get; set; }

        [DisplayName("Friend Name")]
        public string FullNameFriend { get; set; }

        [DisplayName("Friend Phone")]
        public int PhoneFriend { get; set; }

    }
}
