﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Data.Model
{
    public class tblRent
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("IdRent", Order = 0)]
        [DisplayName("ID Rent")]
        public int IdRent { get; set; }

        [Column("IdMember", Order = 1)]
        [DisplayName("Member")]
        public int IdMember { get; set; }

        [Column("Qty", Order = 2)]
        [DisplayName("Quantity")]
        public int Qty { get; set; }
    }
}
