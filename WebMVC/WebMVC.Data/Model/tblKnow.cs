﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMVC.Data.Model
{
    public class tblKnow
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("KnowId", Order = 0)]
        [DisplayName("Source ID")]
        public int KnowId { get; set; }

        [MaxLength(100)]
        [Column("KnowFrom", Order = 1, TypeName = "varchar"), StringLength(100)]
        [DisplayName("Source")]
        public string KnowFrom { get; set; }
    }
}
