﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMVC.Data.Model
{
    public class tblPackage
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("PackageId", Order = 0)]
        [DisplayName("Package ID")]
        public int PackageId { get; set; }

        [MaxLength(20)]
        [Column("PackageName", Order = 1, TypeName = "varchar"), StringLength(20)]
        [DisplayName("Package Name")]
        public string PackageName { get; set; }

        [Column("MaxRent", Order = 2)]
        [DisplayName("Maximal Ren")]
        public int MaxRent { get; set; }

        [Column("CostPerMonth", Order = 3)]
        [DisplayName("Cost Per Month")]
        public int CostPerMonth { get; set; }
    }
}
