﻿using System.Web;
using System.Web.Optimization;

namespace WebMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                     "~/Scripts/gentelella/vendors/jquery/dist/jquery.min.js"));
            //"~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*",
                    "~/Scripts/jquery.validate.unobtrusive.js",
                    "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/bootstrap.js",
                    "~/Scripts/bootstrap-datepicker.min.js",
                    "~/Scripts/bootbox.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/date").Include(
                    "~/Scripts/gentelella/vendors/moment/min/moment.min.js",
                    "~/Scripts/jquery.dateFormat-1.0.js",
                    "~/Scripts/bootstrap-datepicker.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*",
                    "~/Scripts/respond.js"));


            bundles.Add(new ScriptBundle("~/bundles/gantelellajs").Include(
                   "~/Scripts/gentelella/vendors/fastclick/lib/fastclick.js",
                   "~/Scripts/gentelella/vendors/moment/min/moment.min.js",
                   "~/Scripts/gentelella/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                   "~/Scripts/gentelella/vendors/select2/dist/js/select2.full.min.js",
                   "~/Scripts/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js",
                   "~/Scripts/gentelella/vendors/datatables.net/js/dataTables.bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/overlay").Include(
                    "~/Scripts/additional/loadingoverlay/loadingoverlay.min.js",
                    "~/Scripts/additional/loadingoverlay/loadingoverlay_progress.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                    "~/Scripts/helper.js",
                    "~/Scripts/helper-ext.js",
                    "~/Scripts/site.js"));

            bundles.Add(new ScriptBundle("~/bundles/layoutjs").Include(
                    "~/Scripts/gentelella/build/js/custom.js"));


            bundles.Add(new StyleBundle("~/Content/bootstrapcss").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/bootstrap-datepicker.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/Content/gantelellacss").Include(
                    "~/Scripts/gentelella/vendors/font-awesome/css/font-awesome.min.css",
                    "~/Scripts/gentelella/vendors/select2/dist/css/select2.min.css",
                    "~/Scripts/gentelella/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                    "~/Scripts/gentelella/vendors/datatables.net/css/dataTables.bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/layoutcss").Include(
                    "~/Scripts/gentelella/build/css/custom.min.css"));
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
                // note: overrides compilation debug="true" in Web.config
                BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
