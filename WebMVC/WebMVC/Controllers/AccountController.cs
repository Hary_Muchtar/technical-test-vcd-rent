﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebMVC.Models;
using WebMVC.DataRepository.Repo;
using WebMVC.Data.Model;
using WebMVC.Areas;

namespace WebMVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILoginRepository repo;
        public AccountController(ILoginRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(tblLogin model)
        {
            if (repo.cekNipTblSiswa(model.NIP) == 0)
            {
                TempData["Alert"] = "Nip anda belum terdaftar";
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if (repo.cekNipTblLogin(model.NIP) == 0)
                {
                    model.IsAdmin = "0";
                    repo.Add(model);
                    //repo.SaveChanges();
                    var data = repo.getDataSiswaByNIP(model.NIP);
                    varsInfo.NIP = data.FirstOrDefault().NIP;
                    varsInfo.NamaLengkap = data.FirstOrDefault().NamaLengkap;
                    varsInfo.IdKelas = data.FirstOrDefault().IdKelas;
                    varsInfo.Kelas = data.FirstOrDefault().Kelas;
                    Session["UserName"] = varsInfo.NamaLengkap;
                    Session["Kelas"] = varsInfo.Kelas;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    if (repo.cekNipPassworTblLogin(model.NIP, model.Password) == 0)
                    {
                        TempData["Alert"] = "Pastikan NIP dan Password anda benar";
                        return RedirectToAction("Index", "Account");
                    }
                    else
                    {
                        var data = repo.getDataSiswaByNIP(model.NIP);
                        varsInfo.NIP = data.FirstOrDefault().NIP;
                        varsInfo.NamaLengkap = data.FirstOrDefault().NamaLengkap;
                        varsInfo.IdKelas = data.FirstOrDefault().IdKelas;
                        varsInfo.Kelas = data.FirstOrDefault().Kelas;
                        Session["UserName"] = varsInfo.NamaLengkap;
                        Session["Kelas"] = varsInfo.Kelas;
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            Request.Cookies.Clear();
            return RedirectToAction("Login", "Account"); 
        }
    }
}