﻿// Write your Javascript code.
$(function () {
    function getIndexPage() {
        var loc = window.location;
        if (loc.pathname == '/') {
            return window.location;
        }

        var pth = loc.pathname.split('/');
        pth = pth.filter(Boolean);

        var ret = '';
        var len = 1;

        if (pth.length > 3) {
            len = 2;
        }

        for (var i = 0; i < (pth.length - len) ; i++) {
            ret += '/' + pth[i];
        }

        var url = loc.protocol + '//' + loc.host + ret + '/Index';
        return url;
    }

    //https://gasparesganga.com/labs/jquery-loading-overlay/
    $(document).ajaxStart(function () {
        $.LoadingOverlay("show");
    }).ajaxStop(function () {
        $.LoadingOverlay("hide");
    }).ajaxError(function () {
        $.LoadingOverlay("hide");
    });

    //https://select2.github.io/
    $('select').select2();

    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: {
            toDisplay: function (date, format, language) {
                var m = moment(date);
                return m.format('YYYY-MM-DD');
            },
            toValue: function (date, format, language) {
                var m = moment(date);
                return m.toISOString();
            }
        }
    });

    $('.datepicker-icon').click(function () {
        $(".datepicker").datepicker("show");
    });

    //select semua checkbox dalam table
    $('table').on('click', '.select-all', function (e) {
        var table = $(e.target).closest('table').get(0); //$('td:nth-child(1) input:checkbox', table).prop('checked', this.checked);
        var checkboxes = $(table).find('tbody tr input:checkbox:nth-child(1)');
        checkboxes.prop('checked', this.checked);
    });

    //deselect atau select checkbox utama saat td checkbox checked/unchecked
    $('table').on('click', 'tbody input:checkbox:nth-child(1)', function (e) {
        var table = $(e.target).closest('table').get(0);

        var select_all = $('.select-all', table).get(0);
        if (select_all == null || select_all == undefined){
            return;
        }
        
        if (('indeterminate' in select_all)){
            select_all.indeterminate = false;
        }

        var countChecked = $('tbody tr input:checkbox:checked:nth-child(1)', table).length;
        var countAll = $('tbody tr', table).length;

        if (countChecked == countAll) {
            select_all.checked = true;
        }
        else {
            if (countChecked == 0) {
                select_all.checked = false;
                return;
            }

            if (('indeterminate' in select_all)) {
                select_all.indeterminate = true;
            }
            else {
                select_all.checked = false;
            }
        }
    });

    /*
    $(".form-image").submit(function (event) {
        var dataString;
        var action = $("form").attr("action");
        if ($("form").attr("enctype") == "multipart/form-data") {
            event.preventDefault();
            event.stopImmediatePropagation();
            var token = $('input[name="__RequestVerificationToken"]').val();
            //this only works in some browsers.
            //purpose? to submit files over ajax. because screw iframes.
            //also, we need to call .get(0) on the jQuery element to turn it into a regular DOM element so that FormData can use it.
            dataString = new FormData($("form").get(0));
            contentType = false;
            processData = false;

            $.ajax({
                type: "POST",
                url: action,
                data: dataString,
                cache: false,
                headers: { 'RequestVerificationToken': token },
                dataType: "json", //change to your own, else read my note above on enabling the JsonValueProviderFactory in MVC
                contentType: contentType,
                processData: processData,
                beforeSend: function () {
                    return onbegin();
                },
                success: function (response) {
                    showInfo(response.message);
                    //BTW, data is one of the worst names you can make for a variable
                    //handleSuccessFunctionHERE(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //do your own thing
                    showInfo(errorThrown);
                }
            });

            return false;
        } else {
            // regular form, do your own thing if you need it
        }
    }); //end .submit()
    */

    //================================AJAX================================
    function onbegin() {
        var valid = $('form').validate().form();
        return valid;
    }

    function oncomplete(response) {
        var forms = $('form');
        if (forms != null && forms != undefined) {
            //for (var i = 0; i < forms.length; i++) {
            //    forms[i].reset();
            //}
            //forms.find("input[type=text], textarea").val("");
            if (response.status == 200) {
                var msg = response.statusText;
                if (response.responseJSON != undefined) {
                    msg = response.responseJSON.message;
                }

                bootbox.alert({
                    message: msg,
                    size: 'small',
                    title: 'Information',
                    callback: function (result) { //untuk alert, callback result selalu undefined
                        var url = getIndexPage();
                        window.location.href = url;
                        //parent.history.back();
                    }
                });
            }
        }
    }

    function onfailure(xhr, status, statusText) {
        document.open();
        document.write(xhr.responseText);
        document.close();
        //var err = xhr.responseText;
        //var msg = statusText;
        //if (xhr.responseJSON != undefined) {
        //    msg = xhr.responseJSON.message;
        //}

        //bootbox.alert({
        //    message: msg,
        //    size: 'small',
        //    title: 'Status Code ' + xhr.status
        //});
    }
    //====================================================================

    function deleteOnTable(e) {
        if (typeof e !== 'object') {
            throw 'Parameter should be an object of link, parameter, table and token';
            return false;
        }
        var table = $(e.table);
        var tr = table.closest('tr');
        //var id = tr.find("td").eq(0).text();
        bootbox.confirm({
            size: 'small',
            title: 'Delete Confirmation',
            message: 'Are you sure want to delete this data?',
            callback: function (result) {
                if (result) {
                    $.ajax({
                        contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                        dataType: 'json',
                        type: 'POST',
                        url: e.url,
                        headers: { 'RequestVerificationToken': e.token },
                        data: e.parameter,//{ id: id },
                        success: function (data) {
                            bootbox.alert({
                                size: 'small',
                                title: 'Success',
                                message: data.message,
                                callback: function (result) { //untuk confirm result = undefined
                                    if (data.message.indexOf('deleted') > -1) {
                                        tr.remove();
                                    }
                                }
                            });
                        },
                        error: function (xhr, status, text) {
                            document.open();
                            document.write(xhr.responseText);
                            document.close();
                            //bootbox.alert({
                            //    size: "small",
                            //    title: "Error",
                            //    message: a.responseText
                            //})
                        }
                    });
                }
            }
        });
    }

    function editOnTable(e) {
        if (typeof e !== 'object') {
            throw 'Parameter should be an object of link, parameter and optional callback';
            return false;
        }

        var table = $(e.table);
        var tr = table.closest('tr');
        $.ajax({
            type: 'GET',
            url: e.url,
            data: e.parameter,//{ id: id },
            success: function (data) {
                if (data.message) {
                    bootbox.alert({
                        size: 'small',
                        title: 'Message',
                        message: data.message
                    });
                }
                else {
                    window.location.href = data.url;
                }
            },
            error: function (xhr, status, text) {
                document.open();
                document.write(xhr.responseText);
                document.close();
                //bootbox.alert({
                //    size: "small",
                //    title: "Error",
                //    message: a.responseText
                //})
            }
        });
    }


    function ajaxPost(e) {
        if (typeof e !== 'object') {
            throw 'Parameter should be an object of url, parameter, token and optional success callback';
            return false;
        }

        $.ajax({
            url: e.url,
            headers: { 'RequestVerificationToken': e.token },
            type: 'POST',
            data: e.parameter,
            success: function (data) {
                bootbox.alert({
                    size: 'small',
                    title: 'Success',
                    message: data.message,
                    callback: function (result) { //untuk confirm result = undefined
                        if (e.success) {
                            e.success();
                        }
                        else {
                            window.location.reload();
                        }
                    }
                });
            },
            error: function (xhr, status, text) {
                document.open();
                document.write(xhr.responseText);
                document.close();
            }
        });
    }

    function ajaxGet(e) {
        if (typeof e !== 'object') {
            throw 'Parameter should be an object of url, parameter and optional success callback';
            return false;
        }
        $.ajax({
            url: e.url,
            type: 'GET',
            data: e.parameter,
            success: function (data) {
                bootbox.alert({
                    size: 'small',
                    title: 'Success',
                    message: data.message,
                    callback: function (result) { //untuk confirm result = undefined
                        if (e.success) {
                            e.success();
                        }
                    }
                });
            },
            error: function (xhr, status, text) {
                document.open();
                document.write(xhr.responseText);
                document.close();
            }
        });
    }

    function changeTmpColor() {
        if ($('#datatable-tmp').len == 0) {
            return false;
        }
        var len = $('#datatable-tmp').find('tr')[0].cells.length;
        $('#datatable-tmp').find('tbody tr').each(function (i, tr) {
            var val = $(tr).find('td:nth-child(' + len + ')').text();
            switch (val) {
                case 'DELETE':
                    $(tr).css('color', 'red');
                    break;
                case 'UPDATE':
                    $(tr).css('color', '#DAA520');
                    break;
                default:
                    $(tr).css('color', 'blue');
                    break;
            }
        });
    }

    function changeRowItalic(tblTmp) {
        if (tblTmp == null) {
            return false;
        }

        var data = tblTmp.rows().data();

        $('#datatable-buttons').find('tbody tr').each(function (i, tr) {
            var id = $(tr).find('td:nth-child(1)').text();
            //var actype = $(tr).find('td:nth-child(3)').text();

            data.each(function (datum, index) {
                var idx = datum[1];
                if (idx == id) {
                    $(tr).css({ 'font-style': 'italic', 'color': '#800000', 'cursor': 'not-allowed', 'pointer-events': 'none' });
                }
            });
        });
    }

    window.onbegin = onbegin;
    window.oncomplete = oncomplete;
    window.onfailure = onfailure;

    window.editOnTable = editOnTable;
    window.deleteOnTable = deleteOnTable;
    window.ajaxPost = ajaxPost;
    window.ajaxGet = ajaxGet;

    window.changeTmpColor = changeTmpColor;
    window.changeRowItalic = changeRowItalic;
});