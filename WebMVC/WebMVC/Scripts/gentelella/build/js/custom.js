
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args); 
                timeout = null; 
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100); 
        };
    };

    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0];
	
	
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
var setContentHeight = function () {
	// reset height
	$('.right_col').css('min-height', $(window).height());

	var bodyHeight = $('body').outerHeight(),
		footerHeight = $('body').hasClass('footer_fixed') ? -10 : $('footer').height(),
		leftColHeight = $('.left_col').eq(1).height() + $('.sidebar-footer').height(),
		contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

	// normalize content
	contentHeight -= $('.nav_menu').height() + footerHeight;

	$('.right_col').css('min-height', contentHeight);
};

  $('#sidebar-menu').find('a').on('click', function(ev) {
	  console.log('clicked - sidebar_menu');
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $('#sidebar-menu').find('li').removeClass('active active-sm');
                $('#sidebar-menu').find('li ul').slideUp();
            }else
            {
				if ( $('body').is( ".nav-sm" ) )
				{
					$('#sidebar-menu').find( "li" ).removeClass( "active active-sm" );
					$('#sidebar-menu').find( "li ul" ).slideUp();
				}
			}
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

// toggle small or large menu 
$('#menu_toggle').on('click', function() {
		console.log('clicked - menu toggle');
		
		if ($('body').hasClass('nav-md')) {
			$('#sidebar-menu').find('li.active ul').hide();
			$('#sidebar-menu').find('li.active').addClass('active-sm').removeClass('active');
		} else {
			$('#sidebar-menu').find('li.active-sm ul').show();
			$('#sidebar-menu').find('li.active-sm').addClass('active').removeClass('active-sm');
		}

	$('body').toggleClass('nav-md nav-sm');

	setContentHeight();

	$('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
});

	// check active menu
	$('#sidebar-menu').find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

	$('#sidebar-menu').find('a').filter(function () {
		return this.href == CURRENT_URL;
	}).parent('li').addClass('current-page').parents('ul').slideDown(function() {
		setContentHeight();
	}).parent().addClass('active');

	// recompute content when resizing
	$(window).smartresize(function(){  
		setContentHeight();
	});

	setContentHeight();

	// fixed sidebar
	if ($.fn.mCustomScrollbar) {
		$('.menu_fixed').mCustomScrollbar({
			autoHideScrollbar: true,
			theme: 'minimal',
			mouseWheel:{ preventDefault: true }
		});
	}
};

var randNum = function() {
	return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
};

$(document).ready(function() {				
	init_sidebar();				
});