﻿/*
    Arranged By : Ramzi (moh.ramzi@outlook.com)
    Purposes    : jQuery generic functions
    Date        : 04 September 2016
*/

$(function () {
    /*
        arranged by ramzi <moh.ramzi@outlook.com>
    */
    $.fn.jsonToDate = function (a) {
        var regex = /-?\d+/;
        var matches = regex.exec(a);
        var dt = new Date(parseInt(matches[0]));

        var year = dt.getFullYear();

        var month = dt.getMonth() + 1;
        var monthString = month > 9 ? month : '0' + month;

        var day = dt.getDate();
        var dayString = day > 9 ? day : '0' + day;

        var hour = dt.getHours();
        var hourString = hour > 9 ? hour : '0' + hour;

        var minute = dt.getMinutes();
        var minuteString = minute > 9 ? minute : '0' + minute;

        var second = dt.getSeconds();
        var secondString = second > 9 ? second : '0' + second;

        return year + '-' + monthString + '-' + dayString + ' ' + hourString + ':' + minuteString + ':' + secondString;
    }

    $.fn.tableToArray = function () {
        var colLength = this.find('thead tr th').length;
        var rowLength = this.find('tbody tr').length;

        var header = [];
        var retval = [];

        if (rowLength == 0) {
            return header;
        }

        for (var c = 0; c < colLength; c++) {
            var item = this.find("thead tr th")[c].getAttribute('data-model');
            if (!item) {
                item = this.find("thead tr th")[c].innerTex;
            }

            if (item) {
                header.push(item.replace(/ /g, "").trim());
            }
        }

        for (var i = 0; i < rowLength; i++) {
            retval[i] = {};

            for (var c = 0; c < header.length; c++) {
                var element = this.find('tbody tr').eq(i).find('td')[c];

                if (element == undefined || element == null)
                    continue;

                if (element.children != undefined && element.children.length > 0) {
                    var child = element.children[0];

                    switch (child.type) {
                        case 'radio':
                        case 'checkbox':
                            retval[i][header[c]] = child.checked;
                            break;
                        case 'select':
                            retval[i][header[c]] = child.value;
                            break;
                    }
                }
                else {
                    retval[i][header[c]] = element.innerText;
                }
            }
        }

        return retval;
        /*
        var tableRows = [];
        var headersText = [];
        var $headers = this.find("th");
        // Loop through grabbing everything
        var $rows = this.find('tbody tr');

        $rows.each(function (index, row) {
            
            $cells = $(this).find("td");
            tableRows[index] = {};

            $cells.each(function (cellIndex, cell) {
                // Set the header text
                if (headersText[cellIndex] === undefined)
                    headersText[cellIndex] = $($headers[cellIndex]).attr('data-model'); //$($headers[cellIndex]).text();

                var header = headersText[cellIndex];
                if (!header) {
                    header = $($headers[cellIndex]).text().replace(/ /g, "").trim();
                }

                // Update the row object with the header/cell combo
                if (header) {
                    if (cell.children.length > 0) {
                        var child = cell.children[0];
                        switch (child.type) {
                            case 'radio':
                            case 'checkbox':
                                tableRows[index][header] = child.checked;
                                break;
                            case 'select':
                                tableRows[index][header] = child.value;
                                break;
                        }

                    }
                    else {
                        tableRows[index][header] = $(this).text().trim();
                    }
                }
                else {
                    tableRows[index][header] = "";
                }
            });
        });


        //var rowsObject = {
        //    "model": tableRows
        //};
        //var json = JSON.stringify(rowsObject);
        //var str = JSON.stringify(tableRows);

        //var array = $.map(rowsObject, function (value, i) {
        //    return value;
        //});
        //return rowsObject;
        return tableRows;
        */
    }

    $.fn.checkedTableToArray = function () {
        var tableRows = [];
        var headersText = [];
        var $headers = this.find("th");
        // Loop through grabbing everything
        var $rows;
        if (this.find('td:nth-child(1) input[type="checkbox"]').length > 0) {
            $rows = this.find('tbody td:nth-child(1) :input:checkbox:checked').parent().parent();
        }
        else {
            $rows = this.find('tbody tr');
        }

        $rows.each(function (index, row) {
            $cells = $(this).find("td");
            tableRows[index] = {};

            $cells.each(function (cellIndex, cell) {
                // Set the header text
                if (headersText[cellIndex] === undefined)
                    headersText[cellIndex] = $($headers[cellIndex]).attr('data-model'); //$($headers[cellIndex]).text();

                var header = headersText[cellIndex];
                if (!header) {
                    header = $($headers[cellIndex]).text().replace(/ /g, "").trim();
                }

                // Update the row object with the header/cell combo
                if (header) {
                    if (cell.children.length > 0) {
                        var child = cell.children[0];
                        switch (child.type) {
                            case 'radio':
                            case 'checkbox':
                                tableRows[index][header] = child.checked;
                                break;
                            case 'select':
                                tableRows[index][header] = child.value;
                                break;
                        }

                    }
                    else {
                        tableRows[index][header] = $(this).text().trim();
                    }
                }
            });
        });


        //var rowsObject = {
        //    "model": tableRows
        //};
        //var json = JSON.stringify(rowsObject);
        //var str = JSON.stringify(tableRows);

        //var array = $.map(rowsObject, function (value, i) {
        //    return value;
        //});
        //return rowsObject;
        return tableRows;
    }

    $.fn.checkedTableIdToArray = function (a) {
        var arrayOfValues = [];
        var ch = this.find('input[type="checkbox"]:nth-child(1):checked');
        ch.each(function (i) {
            var element = $(ch).get(i)
            element = $(element).closest('tr');
            element = $(element).find('td').eq(a).text();
            arrayOfValues.push(element);
        });

        return arrayOfValues;
    }

    $.fn.serializeAnything = function () {

        var toReturn = [];
        var els = $(this).find('input, select, textarea').get();

        $.each(els, function () {
            if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|number|date|hidden|password/i.test(this.type))) {
                var val = $(this).val();
                var obj = {};
                obj['name'] = this.name;
                obj['value'] = val;
                //toReturn.push(encodeURIComponent(this.name) + "=" + encodeURIComponent(val));
                toReturn.push(obj);
            }
        });

        //return toReturn.join("&").replace(/%20/g, "+");
        return toReturn;
    }

    $.fn.formToObject = function () {
        var o = {};
        var a = this.serializeArray();

        if (a == null || a.length == 0) {
            //a = this.find('input').serialize();
            a = this.serializeAnything();
        }
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {

                var name = this.name;
                //jika nama model seperti OrderViewModel.OrderDetail
                if (this.name.indexOf(".") > 0) {
                    var names = this.name.split('.');

                    if (names.length > 0) {
                        name = names[names.length - 1];
                    }
                }

                o[name] = this.value || '';
            }
        });
        return o;
    };

    $.fn.appenRow = function (obj) {
        var ob = obj;

        var isArray = Object.prototype.toString.call(obj) === '[object Array]'
        var isObject = typeof obj == 'object';

        if (!isObject && isArray) {
            ob = $.extend({}, obj);
        }

        var row = '<tr>';
        var names = $('[data-model]').map(function (i, el) {
            var modelName = $(el).data('model');
            var value = ob[modelName];
            if (value == undefined) {
                value = '';
            }

            row += ('<td>' + value + '</td>');
        }).get();

        row += '<td><a href="#">Remove</a></td></tr>';
        this.find("tbody").append(row);
    };

    $.fn.appendTable = function (objArray) {
        this.find('tbody').empty();

        var objects = [];
        var types = getType(objArray);

        if (getType(objArray) == 'string') {
            objects = $.parseJSON(objArray);
        }

        var headers = this.find("th");

        var tr = '';
        for (var i = 0; i < objects.length; i++) {
            tr = '<tr>';
            var td = '';
            for (var prop in objects[i]) {
                headers.each(function (i, cell) {
                    var header = $(cell).attr('data-model');
                    if (!header)
                        header = $(cell).text().replace(/ /g, "").trim();
                    if (header == prop) {
                        td += '<td>' + objects[i][prop] + '</td>';
                    }
                });
            }
            tr += td + '</tr>';
        }
        this.find("tbody").append(tr);
    }

    function exception(message) {
        this.message = message;
        this.error = 'Error occured while trying to access this function';
    }

    //fungsi untuk populate json ke form sesuai dengan input type
    $.fn.populate = function (data) {
        $this = this;
        $.each(data, function (key, value) {
            var $ctrl = $('[name=' + key + ']', $this); //http://stackoverflow.com/questions/6933763/how-do-i-select-item-with-class-within-a-div-in-jquery
            //var $ctrl = $this.find($('[name=' + key + ']'));
            if ($ctrl.tagName == 'SELECT') {
                $ctrl.val(value);
            }
            switch ($ctrl.attr("type")) {
                case "text": case "number": case "hidden":
                    $ctrl.val(value);
                    break;
                case "radio": case "checkbox":
                    $ctrl.each(function () {
                        if ($(this).attr('value') == value) { $(this).attr("checked", value); }
                    });
                    break;
                case "date":
                    $ctrl.val(convertJsonDateString(value));
                    break;
                default:
                    $ctrl.val(value);
                    break;
            }
        });
    }

    $.fn.serializeAnything = function () {

        var toReturn = [];
        var els = $(this).find('input, select, textarea').get();

        $.each(els, function () {
            if (this.name && !this.disabled && (this.checked || /select|textarea/i.test(this.nodeName) || /text|number|date|hidden|password/i.test(this.type))) {
                var val = $(this).val();
                var obj = {};
                obj['name'] = this.name;
                obj['value'] = val;
                //toReturn.push(encodeURIComponent(this.name) + "=" + encodeURIComponent(val));
                toReturn.push(obj);
            }
        });

        //return toReturn.join("&").replace(/%20/g, "+");
        return toReturn;
    }

    //extend createTable dengan object parameter
    //$.fn.extend({
    //    createTable: function (parameter) {
    //        if (parameter !== null && typeof parameter === 'object') {
    //            return this.createTable();
    //        }
    //        else {
    //            throw new exception('Parameter must be an object with jsondata, headers if any and attributes if any');
    //        }
    //    }
    //});

    //bikin element <table> dari json array
    $.fn.createTable = function (parameter) {
        var json;
        var headers;
        var attributes;

        if (parameter !== null && typeof parameter === 'object') {
            json = parameter.json;
            headers = parameter.headers;
            attributes = parameter.attributes;
        }
        else {
            throw new exception('Parameter must be an object with json data, headers if any and attributes if any');
        }
        //append table ke element
        $table = '<table>';

        var captioned = false;
        if (headers === undefined || headers == null) {
            headers = [];
        }

        var headersx = []; //{"caption": "abc", "property" : "cba" }
        headers.forEach(function (caption) {
            var obj = {};
            obj.caption = caption;
            obj.property = null;
            headersx.push(obj)
        });

        //loop json array. array of object
        $.each(json, function (index, value) {
            //ambil object property sebagai caption
            if (!captioned) {
                if (headersx.length == 0) {
                    for (var propName in value) {
                        var obj = {};
                        obj.caption = propName;
                        obj.property = propName;
                        headersx.push(obj)
                    }
                }
                else {
                    var i = 0;
                    for (var propName in value) {
                        if (headersx.length > i) {
                            headersx[i].property = propName;
                        }
                        i = i + 1;
                    }
                }

                $table += '<thead><tr>';
                headersx.forEach(function (hd) {
                    $table += '<th data-model="' + hd.property + '">' + hd.caption + '</th>';
                });
                $table += '</tr></thead><tbody>';
                captioned = true;
            }

            //if (!captioned) {
            //    if (headers.length == 0) {
            //        for (var propName in value) {
            //            headers.push(propName);
            //        }
            //    }
            //    $table += '<thead><tr>';
            //    headers.forEach(function (caption) {
            //        $table += '<th>' + caption + '</th>';
            //    });
            //    $table += '</tr></thead><tbody>';
            //    captioned = true;
            //}

            //masing masing object jadiin array
            var arr = $.map(value, function (ar, index) {
                return ar;
            })

            $table += '<tr>';
            for (var i = 0; i < arr.length; i++) {
                $table += '<td>' + arr[i] + '</td>';
            }
            $table += '</tr>';
        });
        $table += '</tbody></table>';

        this.append($table);
        if (attributes !== null && typeof attributes === 'object') {
            var tbl = this.find('table');
            if (tbl !== undefined)
                tbl.attr(attributes);
        }
        return $table;
    }

    $.fn.appenRow = function (obj) {
        var ob = obj;

        var isArray = Object.prototype.toString.call(obj) === '[object Array]'
        var isObject = typeof obj == 'object';

        if (!isObject && isArray) {
            ob = $.extend({}, obj);
        }

        var row = '<tr>';
        var names = $('[data-model]').map(function (i, el) {
            var modelName = $(el).data('model');
            var value = ob[modelName];
            if (value == undefined) {
                value = '';
            }

            row += ('<td>' + value + '</td>');
        }).get();

        row += '<td><a href="#">Remove</a></td></tr>';
        this.find("tbody").append(row);
    };

    $.fn.appendTable = function (objArray) {
        this.find('tbody').empty();

        var objects = [];
        var types = getType(objArray);

        if (getType(objArray) == 'string') {
            objects = $.parseJSON(objArray);
        }

        var headers = this.find("th");

        var tr = '';
        for (var i = 0; i < objects.length; i++) {
            tr = '<tr>';
            var td = '';
            for (var prop in objects[i]) {
                headers.each(function (i, cell) {
                    var header = $(cell).attr('data-model');
                    if (!header)
                        header = $(cell).text().replace(/ /g, "").trim();
                    if (header == prop) {
                        td += '<td>' + objects[i][prop] + '</td>';
                    }
                });
            }
            tr += td + '</tr>';
        }
        this.find("tbody").append(tr);
    }

    $.fn.toNumber = function () {
        var el = this[0];
        var value = '';
        if (el.value !== undefined) {
            value = $(el).val();
        } else {
            value = $(el).html();
        }

        if (value) {
            value = value.replace(/\./g, '');
            value = value.replace(',', '');
            return parseFloat(value);
        }
        else {
            return 0;
        }
    };

    $.fn.validateNumber = function () {
        this.find("input.number").each(function (i, e) {
            var input = $(e); // This is the jquery object of the input, do what you will
            input.removeClass('number');
            input.addClass('xnumber');
            var number = $(input).toNumber();
            $(input).val(number);
        });
    };

    $.fn.revalidateNumber = function () {
        this.find("input.xnumber").each(function (i, e) {
            var input = $(e); // This is the jquery object of the input, do what you will
            input.addClass('number');
            input.trigger('keyup');
        });
    };
});

//(function ($) {
//    $.toNumber = function () {
//        var value = this.val();
//        if (value) {
//            value = value.replace(/\./g, '');
//            value = value.replace(',', '');
//            return parseFloat(value);
//        }
//        else {
//            return 0;
//        }
//    };
//})(jQuery);