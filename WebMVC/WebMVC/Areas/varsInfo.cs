﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMVC.Areas
{
    public class varsInfo
    {
        public static int Urutan { get; set; }
        public static int NIP { get; set; }
        public static string NamaLengkap { get; set; }
        public static int? IdKelas { get; set; }
        public static string Kelas { get; set; }
    }
}