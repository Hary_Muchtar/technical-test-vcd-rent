﻿using System.Web.Mvc;
using WebMVC.Data.Model;
using WebMVC.DataRepository.Repo;

namespace WebMVC.Areas.Admin.Controllers
{
    public class RentController : Controller
    {
        // GET: Admin/Rent
        private readonly IRentRepository repo;
        public RentController(IRentRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            var data = repo.Read();
            return View(data);
        }
        public ActionResult Add()
        {
            ViewBag.SelectMember = repo.getMember();
            return View();
        }
        [HttpPost]
        public ActionResult Add(tblRent model)
        {
            string _actived = "";
            _actived = repo.cekActiveUser(model.IdMember);
            if (_actived == "Yes")
            {
                repo.Add(model);
                repo.SaveChanges();
                return Json(new { message = "Record has been inserted" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Member Not Actived" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}