﻿using System.Web.Mvc;
using WebMVC.Data.Model;
using WebMVC.DataRepository.Repo;

namespace WebMVC.Areas.Admin.Controllers
{
    public class KnowController : Controller
    {
        // GET: Admin/Know
        private readonly IKnowRepository repo;
        public KnowController(IKnowRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            var data = repo.Read();
            return View(data);
        }
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(tblKnow model)
        {
            repo.Add(model);
            repo.SaveChanges();
            return Json(new { message = "Record has been inserted" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            tblKnow model = repo.getById(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(tblKnow model)
        {
            var data = repo.getById(model.KnowId);
            data.KnowFrom = model.KnowFrom;
            
            repo.Edit(data);
            repo.SaveChanges();
            return Json(new { message = "Record has been updated" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {
            tblKnow model = repo.getById(id);
            repo.Delete(model);
            repo.SaveChanges();
            return Json(new { message = "Record has been deleted" }, JsonRequestBehavior.AllowGet);
        }
    }
}