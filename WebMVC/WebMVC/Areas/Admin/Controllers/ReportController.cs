﻿using System.Web.Mvc;
using WebMVC.Data.Model;
using WebMVC.DataRepository.Repo;

namespace WebMVC.Areas.Admin.Controllers
{
    public class ReportController : Controller
    {
        // GET: Admin/Report
        private readonly IReferalRepository repo;
        public ReportController(IReferalRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            var data = repo.ReadReport();
            return View(data);
        }
    }
}