﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMVC.Data.Model;
using WebMVC.Data.Model.ViewModel;
using WebMVC.DataRepository.Repo;

namespace WebMVC.Areas.Admin.Controllers
{
    public class MemberController : Controller
    {
        // GET: Admin/Member
        private readonly IMemberRepository repo;
        public MemberController(IMemberRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            var data = repo.Read();
            return View(data);
        }
        public ActionResult Add()
        {
            ViewBag.SelectSource = repo.getSourceName();
            ViewBag.SelectPackage = repo.getPackageName();
            
            return View();
        }
        [HttpPost]
        public ActionResult Add(viewMember model)
        {
            if (model.Age > 16)
            {
                tblMember _member = new tblMember();
                tblReferal _ref = new tblReferal();
                int Id = 0;
                _member.IdentityNumber = model.IdentityNumber;
                _member.FullNameMember = model.FullNameMember;
                _member.Address = model.Address;
                _member.PhoneMember = model.PhoneMember;
                _member.Age = model.Age;
                _member.AnotherMembership = model.AnotherMembership;
                _member.KnowId = model.KnowId;
                _member.PackageId = model.PackageId;
                _member.RegistrationDate = DateTime.Now;
                _member.IsActive = "No";
                repo.AddMember(_member);
                repo.SaveChanges();

                Id = repo.getMaxIdMember();
                _ref.IdMember = Id;
                _ref.FullNameFriend = model.FullNameFriend1;
                _ref.PhoneFriend = model.PhoneFriend1;
                repo.AddReferal(_ref);

                _ref.IdMember = Id;
                _ref.FullNameFriend = model.FullNameFriend2;
                _ref.PhoneFriend = model.PhoneFriend2;
                repo.AddReferal(_ref);
                //repo.SaveChanges();

                return Json(new { message = "Record has been inserted" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Age must be older than 16" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Edit(int id)
        {
            tblMember model = repo.getById(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(tblMember model)
        {
            //var data = repo.getById(model.IdMember);
            var _model = repo.getById(model.IdMember);
            _model.IsActive = model.IsActive;
            

            repo.EditMember(_model);
            repo.SaveChanges();
            return Json(new { message = "Record has been updated" }, JsonRequestBehavior.AllowGet);
        }
    }
}