﻿    using System.Web.Mvc;
    using WebMVC.Data.Model;
    using WebMVC.DataRepository.Repo;

namespace WebMVC.Areas.Admin.Controllers
{
    public class PackageController : Controller
    {
        //
        // GET: /Admin/Package/
        private readonly IPackageRepository repo;
        public PackageController(IPackageRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            var data = repo.Read();
            return View(data);
        }
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(tblPackage model)
        {
            repo.Add(model);
            repo.SaveChanges();
            return Json(new { message = "Record has been inserted" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            tblPackage model = repo.getById(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(tblPackage model)
        {
            var data = repo.getById(model.PackageId);
            data.PackageName = model.PackageName;
            data.MaxRent = model.MaxRent;
            data.CostPerMonth = model.CostPerMonth;

            repo.Edit(data);
            repo.SaveChanges();
            return Json(new { message = "Record has been updated" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {
            tblPackage model = repo.getById(id);
            repo.Delete(model);
            repo.SaveChanges();
            return Json(new { message = "Record has been deleted" }, JsonRequestBehavior.AllowGet);
        }
    }
}