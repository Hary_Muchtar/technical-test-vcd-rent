﻿using System.Web.Mvc;
using WebMVC.Data.Model;
using WebMVC.DataRepository.Repo;
namespace WebMVC.Areas.Admin.Controllers
{
    public class ReferalController : Controller
    {
        // GET: Admin/Referal
        private readonly IReferalRepository repo;
        public ReferalController(IReferalRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {
            var data = repo.Read();
            return View(data);
        }
        public ActionResult Add()
        {
            ViewBag.SelectMember = repo.getMember();
            return View();
        }
        [HttpPost]
        public ActionResult Add(tblReferal model)
        {
            int validate = 0;
            validate = repo.cekDataReady(model.FullNameFriend, model.PhoneFriend);
            if (validate == 0)
            {
                repo.Add(model);
                repo.SaveChanges();
                return Json(new { message = "Record has been inserted" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Name and Phone Number Already Exist" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Edit(int id)
        {
            tblReferal model = repo.getById(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(tblReferal model)
        {
            var data = repo.getById(model.IdRefereal);
            data.IdMember = model.IdMember;
            data.FullNameFriend = model.FullNameFriend;
            data.PhoneFriend = model.PhoneFriend;

            repo.Edit(data);
            repo.SaveChanges();
            return Json(new { message = "Record has been updated" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {
            tblReferal model = repo.getById(id);
            repo.Delete(model);
            repo.SaveChanges();
            return Json(new { message = "Record has been deleted" }, JsonRequestBehavior.AllowGet);
        }
    }
}