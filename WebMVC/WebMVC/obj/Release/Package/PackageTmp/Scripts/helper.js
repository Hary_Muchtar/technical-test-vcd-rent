﻿/*
    Arranged By : Ramzi (moh.ramzi@outlook.com)
    Purposes    : jQuery generic functions
    Date        : 04 September 2016
*/

$(function () {
    function convertJsonDateString(jsonDate) {
        var splits = String(jsonDate).split(' ');
        if (splits.length > 5) {
            var mnths = {
                Jan: "01", Feb: "02", Mar: "03", Apr: "04", May: "05", Jun: "06", Jul: "07", Aug: "08", Sep: "09", Oct: "10", Nov: "11", Dec: "12"
            };
            return [splits[3], mnths[splits[1]], splits[2]].join("-");
        }
        else {
            var shortDate = null;
            if (jsonDate) {
                var regex = /-?\d+/;
                var matches = regex.exec(jsonDate);
                var dt = new Date(parseInt(matches[0]));
                var month = dt.getMonth() + 1;
                var monthString = month > 9 ? month : '0' + month;
                var day = dt.getDate();
                var dayString = day > 9 ? day : '0' + day;
                var year = dt.getFullYear();
                shortDate = year + '-' + monthString + '-' + dayString;
            }
            return shortDate;
        }
    };

    function getType(variable) {
        var stringConstructor = "test".constructor;
        var arrayConstructor = [].constructor;
        var objectConstructor = {}.constructor;

        if (variable === null) {
            return "null";
        }
        else if (variable === undefined) {
            return "undefined";
        }
        else if (variable.constructor === stringConstructor) {
            return "string";
        }
        else if (variable.constructor === arrayConstructor) {
            return "array";
        }
        else if (variable.constructor === objectConstructor) {
            return "abject";
        }
        else {
            return "unknown";
        }
    }

    function parseNumeric(value) {
        if (value) {
            value = value.replace(/\./g, '');
            value = value.replace(',', '');
            return parseFloat(value);
        }
        else {
            return 0;
        }
    }

    window.convertJsonDateString = convertJsonDateString;
    window.getType = getType;
    window.parseNumeric = parseNumeric;
});
